# hello world kpt

## Description

  Sample hello world package using custom setters for customization.

  Create workspace and get the package:

    $ mkdir hello-world-workspace; cd $_
    $ export SRC_REPO=https://gitlab.com/bengordonfraser/hello-world-kpt.git
    $ kpt pkg get $SRC_REPO@v0.1.0 helloworld                                                                                                     
        fetching package "/" from "https://gitlab.com/bengordonfraser/hello-world-kpt" to "helloworld"


  List the package contents:

    $ kpt cfg tree helloworld
        helloworld
        ├── [Kptfile]  Kptfile hello-world-set
        ├── [deploy.yaml]  Deployment helloworld
        ├── [ingress.yaml]  Ingress helloworld
        ├── [inventory-template.yaml]  ConfigMap default/inventory-29314022
        └── [service.yaml]  Service helloworld


  List the package setters:

    $ kpt cfg list-setters helloworld 
      helloworld/
          NAME        VALUE          SET BY             DESCRIPTION        COUNT   REQUIRED   IS SET  
        http-port   80                             helloworld port         3       No         Yes     
        image-tag   v0.3.0       package-default   hello-world image tag   1       No         No      
        name        helloworld                                             5       No         Yes     
        replicas    2                              helloworld replicas     1       No         Yes 


  Set a field:

    $ kpt cfg set helloworld replicas 5
        helloworld/
        set 1 field(s) of setter "replicas" to value "5"


  View the changes:

    $ kpt cfg tree helloworld  --replicas
        helloworld
        ├── [Kptfile]  Kptfile hello-world-set
        ├── [deploy.yaml]  Deployment helloworld
        │   └── spec.replicas: 5
        ├── [ingress.yaml]  Ingress helloworld
        ├── [inventory-template.yaml]  ConfigMap default/inventory-29314022
        └── [service.yaml]  Service helloworld

  Apply the package to a cluster:

    $ kpt live apply helloworld 
        service/helloworld created
        deployment.apps/helloworld created
        ingress.networking.k8s.io/helloworld created
        3 resource(s) applied. 3 created, 0 unchanged, 0 configured, 0 failed
        0 resource(s) pruned, 0 skipped, 0 failed

  Clean up: 

    $ kpt live destroy helloworld
        ingress.networking.k8s.io/helloworld deleted
        deployment.apps/helloworld deleted
        service/helloworld deleted
        3 resource(s) deleted, 0 skipped

## Read more: 

https://googlecontainertools.github.io/kpt/

https://bengordonfraser.medium.com/hello-world-with-terraform-k3d-and-kpt-860e8a414669
